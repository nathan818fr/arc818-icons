#!/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit
cd "$(dirname "$(readlink -f "$0")")"

function main() {
  build_dir="$(pwd)/build"
  out_dir="${build_dir}/out"

  case "${1:-}" in
  clean) clean "$@" ;;
  build) build "$@" ;;
  package) package "$@" ;;
  install) install "$@" ;;
  *)
    echo "Usage: $0 clean|build|package|install" >&2
    exit 1
    ;;
  esac
}

function clean() {
  rm -rf "$build_dir"
}

function build() {
  mkdir -p "$build_dir"

  # preconditions checks
  # TODO (pre-check that all commands/packages are available)

  # build local theme
  _sync 'src' "${out_dir}/Arc818"

  # build arc-icon-theme
  _fast_clone 'git@github.com:horst3180/arc-icon-theme.git' '55a575386a412544c3ed2b5617a61f842ee4ec15' "${build_dir}/arc-icon-theme"
  pushd "${build_dir}/arc-icon-theme" >/dev/null
  {
    rm 'Arc/apps/symbolic/system-file-manager-symbolic.svg'
    find 'Arc' -type f -name '*.png' -exec optipng -strip all -silent {} \;
    _rewrite_index 'Arc/index.theme' 'Arc818-child01'
    _sync 'Arc' "${out_dir}/Arc818-child01"
  }
  popd >/dev/null

  # build elementary-xfce
  _fast_clone 'git@github.com:shimmerproject/elementary-xfce.git' '2e2ca316322f9c69a8fb5d555f42d121ba5412b4' "${build_dir}/elementary-xfce"
  pushd "${build_dir}/elementary-xfce" >/dev/null
  {
    ./configure --prefix="$(pwd)/out"
    make

    _rewrite_index 'build/elementary-xfce-dark/index.theme' 'Arc818-child02'
    _sync 'build/elementary-xfce-dark' "${out_dir}/Arc818-child02"

    _rewrite_index 'build/elementary-xfce/index.theme' 'Arc818-child03'
    _sync 'build/elementary-xfce' "${out_dir}/Arc818-child03"
  }
  popd >/dev/null

  echo 'Done'
}

function package() {
  tar -cv -C "$out_dir" --transform 's,^\.,Arc818-icons,' . | gzip >"Arc818-icons.tar.gz"
}

function install() {
  while IFS= read -r -d '' file; do
    dst="/usr/local/share/icons/$(basename "$file")"
    set -x
    sudo rm -rf "$dst"
    sudo cp -TR "$file" "$dst"
    { set +x; } 2>/dev/null
  done < <(find "${build_dir}/out" -mindepth 1 -maxdepth 1 -type d -print0)

  echo 'Done'
}

function _fast_clone() {
  local repo commit dst
  repo="$1"
  commit="$2"
  dst="$3"

  rm -rf "$dst"
  mkdir -p "$dst"
  git -C "$dst" init -b master >&2
  git -C "$dst" remote add origin "$repo" >&2
  git -C "$dst" fetch --depth 1 origin "$commit" >&2
  git -C "$dst" checkout FETCH_HEAD >&2
}

function _rewrite_index() {
  local index_file name inherits
  index_file="$1"
  name="$2"
  inherits="${3:-}"

  sed -i -E 's/^Name=.+/Name='"$name"'/' "$index_file"
  sed -i -E 's/^Inherits=.+/Inherits='"$inherits"'/' "$index_file"
}

function _sync() {
  local src dst
  src="$1"
  dst="$2"

  rm -rf -- "$dst"
  mkdir -p -- "$dst"
  cp -RT -- "$src" "$dst"
}

main "$@"
exit "$?"
